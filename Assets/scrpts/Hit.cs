﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hit : MonoBehaviour {

	public Collider2D ShooterCapsuleCollider;
	public float Damage;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D otherCollider)
    {
        NewMethod(otherCollider);

    }


    private void NewMethod(Collider2D otherCollider)
    {
        if (otherCollider.name == "UnitModel")
        {
            if (otherCollider.gameObject != ShooterCapsuleCollider.gameObject)
            {
                var unitHealthPoints = otherCollider.gameObject.GetComponentInParent<HealthPoints>();

                unitHealthPoints.ChangeHealthPoints(-Damage);
                Destroy(gameObject);
            }
            else
                return;
        }
        Destroy(gameObject);

    }
}
