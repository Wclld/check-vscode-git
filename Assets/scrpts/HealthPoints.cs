﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPoints : MonoBehaviour {

	public float Max_HealthPoints;
	float CurrentHealthPoints;

	ChangeHealthBar HealthBar;
	// Use this for initialization
	void Start () {
		CurrentHealthPoints = Max_HealthPoints;
		HealthBar = gameObject.GetComponentInChildren<ChangeHealthBar>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ChangeHealthPoints(float value)
	{
		CurrentHealthPoints += value;
		var healthPercent = CurrentHealthPoints / Max_HealthPoints;
		HealthBar.ChangeBar(healthPercent);
		if(CurrentHealthPoints <= 0 )
		{
			Destroy(gameObject);
		} 
	}
}
