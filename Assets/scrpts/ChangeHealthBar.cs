﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeHealthBar : MonoBehaviour {
	
	public GameObject healthBar;

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void ChangeBar(float healthPercent)
	{
		var localScale = healthBar.transform.localScale;
		localScale = new Vector3(healthPercent, localScale.y, localScale.z);
        healthBar.transform.localScale = localScale;
	}
}
