﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotting : MonoBehaviour
{

    public Rigidbody2D bullet;
    public Camera camera;
    public float Speed;
    Collider2D capsuleCollider;

    private float timestomp;
    void Start()
    {
        capsuleCollider = GetComponent<CapsuleCollider2D>();
        camera = GameObject.FindObjectOfType<Camera>();
    }
    void Update()
    {
        if (Input.GetMouseButton(1) && Time.time >= timestomp)
        {
            Fire();

        }
    }

    void Fire()
    {
        Vector2 pointerPosition = camera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 aimVector = pointerPosition - (Vector2)transform.position;

        var XYSum =  Mathf.Abs(aimVector.x) + Mathf.Abs(aimVector.y);

        var xPercentage = Mathf.Abs(aimVector.x) / XYSum;

        var newX = Speed * xPercentage;
        var newY = Speed - newX;
        if (aimVector.x < 0)
            newX *= -1;
        if (aimVector.y < 0)
            newY *= -1;
        aimVector.x = newX;
        aimVector.y = newY;


        var shootedBullet = Instantiate(bullet, transform.position, transform.rotation);
        var hit = shootedBullet.gameObject.GetComponent<Hit>();
        hit.ShooterCapsuleCollider = capsuleCollider;
        shootedBullet.velocity = aimVector;



        timestomp = Time.time + 0.33f;
    }
}
