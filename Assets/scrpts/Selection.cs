﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Selection : MonoBehaviour {

    private Vector3 MouseStartPosition;
    private Vector3 MouseEndPosition;

    private List<GameObject> SelectableTargets;
    Camera camera;


    // Use this for initialization
    void Start ()
    {
        SelectableTargets = GetSelectableObjects();
        camera = GetComponent<Camera>();

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            MouseStartPosition = Input.mousePosition;
            
        }
        if (Input.GetMouseButtonUp(0))
        {
            MouseEndPosition = Input.mousePosition;
            var maxMousePosition = Vector3.Max(MouseStartPosition,MouseEndPosition);
            var minMousePosition = Vector3.Min(MouseStartPosition, MouseEndPosition);

            foreach (var target in SelectableTargets)
            {
                var targetPosition = camera.WorldToScreenPoint(target.transform.position);
                if (IsBetween(maxMousePosition, minMousePosition, targetPosition))
                    Debug.Log(String.Format("{0}- is in",target.name));
            }

        }

    }

    List<GameObject> GetSelectableObjects()
    {
        var a = GameObject.FindGameObjectsWithTag("SelectableObjects");
        var b = a.OfType<GameObject>().ToList();

        return b;
    }

    bool IsBetween(Vector3 maxMousePosition, Vector3 minMousePosition, Vector3 objectPosition)
    {
        if (minMousePosition.x <= objectPosition.x &&
            minMousePosition.y <= objectPosition.y &&
            objectPosition.y <= maxMousePosition.y &&
            objectPosition.x <= maxMousePosition.x)
            return true;
        return false;

    }
}
