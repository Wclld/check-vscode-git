﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    public float moveSpeed;
    public Rigidbody2D rb;
    public bool isSelected;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (isSelected)
        {
            var horizontalMovement = Input.GetAxis("Horizontal") * moveSpeed;
            var vericalMovement = Input.GetAxis("Vertical") * moveSpeed;
            rb.velocity = new Vector2(horizontalMovement, vericalMovement);
        }
    }
}
