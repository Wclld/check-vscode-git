﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDirecting : MonoBehaviour {

    public Camera camera;
	// Use this for initialization
	void Start ()
    {
        camera = GameObject.FindObjectOfType<Camera>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        var dAngel = transform.rotation.eulerAngles.z;

        Vector2 thisPosition = transform.position;
        Vector2 pointerPosition = camera.ScreenToWorldPoint(Input.mousePosition);

        var direction = Vector2.Angle(pointerPosition - thisPosition, Vector2.up);
        if (pointerPosition.x > thisPosition.x)
            direction = 360 - direction;
        transform.Rotate(0, 0, direction - dAngel);

    }
}
